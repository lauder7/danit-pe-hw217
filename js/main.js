lbMain: {
    const nGoodMarkThreshold = 4;
    const nScholarshipMinAvgMark = 7;
    let oReportCard = {};

    oReportCard.firstName = inputString("Student Record Card analysis. Enter Student's first name");
    if (oReportCard.firstName === null) break lbMain;
    
    oReportCard.lastName = inputString("Enter Student's last name");
    if (oReportCard.lastName === null) break lbMain;

    oReportCard.scoreSheet = {};

    let sSubject = "";
    let nMark = 0;
    while (true) {
        sSubject = inputString(`Enter Student's subject`);
        if (sSubject === null) break;

        nMark = inputInteger(`Enter Student's mark in ${sSubject}`);
        if (nMark === null) break;

        oReportCard.scoreSheet[sSubject] = nMark;
    }

    let nSubjects = 0;
    let nTotalScore = 0;
    let nBadMarks = 0;
    for (let k in oReportCard.scoreSheet) {
        nSubjects++;
        nTotalScore += oReportCard.scoreSheet[k];
        if (oReportCard.scoreSheet[k] < nGoodMarkThreshold) nBadMarks++;
    }

    if (!nSubjects) {
        alert(`No subjects have been entered`);
        break lbMain;
    }

    if (!nBadMarks) 
        alert(`${oReportCard.firstName} ${oReportCard.lastName} was promoted to the next class`);

    if (nTotalScore / nSubjects > nScholarshipMinAvgMark)
        alert(`The scholarship was awarded to ${oReportCard.firstName} ${oReportCard.lastName}`);
}




function inputString(sMsg) {
    let sBuffer = "";

    do {
        sBuffer = prompt(sMsg, sBuffer);
    } while (sBuffer !== null && !sBuffer)

    return sBuffer;
}




function inputInteger(sMsg, nFrom = 1, nTo = Infinity) {
    let sNum = "";
    let nNum;

    do {
        sNum = prompt(sMsg, sNum);
        if (sNum === null) return null;
        nNum = +sNum;
    } while (sNum === "" || Number.isNaN(nNum) || !Number.isInteger(nNum) || nNum < nFrom || nNum > nTo);
    
    return nNum;
}

